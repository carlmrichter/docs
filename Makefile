# HELP
# This will output the help for each task
.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help
EXTRACT_DIR := ./craft.tmp
INSTALL_DIR := ./src

# DOCKER TASKS
# Build the project and the containers
build: ## Build the project with composer and set up the docker containers
	git clone --depth=1 --branch=master https://github.com/craftcms/craft.git $(EXTRACT_DIR)
	rm -rf $(EXTRACT_DIR)/.git
	rm $(EXTRACT_DIR)/LICENSE.md
	rm $(EXTRACT_DIR)/README.md
	rm $(EXTRACT_DIR)/composer.json
	mv $(EXTRACT_DIR)/composer.json.default $(EXTRACT_DIR)/composer.json
	cp $(EXTRACT_DIR)/.env.example $(EXTRACT_DIR)/.env
	rsync -rvh --ignore-existing $(EXTRACT_DIR)/. $(INSTALL_DIR)
	rm -rf $(EXTRACT_DIR)
	composer install --ignore-platform-reqs -d $(INSTALL_DIR)
	$(INSTALL_DIR)/craft setup/security-key
	docker-compose build

refresh: ## Refresh composer dependencies
	composer install -d $(INSTALL_DIR)

# Start the containers
up: ## Bring the docker environment up
	docker-compose up -d

# Stop the containers
down: ## Bring the docker environment down
	docker-compose down
