# [docs.carlrichter.de](https://docs.carlrichter.de/)

Webdeveloper Docs managed with [Craft CMS](https://craftcms.com)
  
## Useful Links

### General

- [Craft CMS Docs](https://docs.craftcms.com/)
- [Prism.js](https://prismjs.com/) - Code Highlighting
- [Element API Plugin for Craft](https://github.com/craftcms/element-api)

### GitLab CI/CD

- [Stylelint Docs](https://stylelint.io/)