<?php

use craft\elements\Entry;
use craft\helpers\UrlHelper;
use Craft;

function extractQueryString() {
    $queryString = '';
    $title = Craft::$app->request->getParam('title');
    if (!empty($title)) $queryString .= $title . ' ';
    $queryString .= Craft::$app->request->getParam('q');
    return $queryString;
}

return [
    'endpoints' => [
        'api/search.json' => function () {
            return [
                'elementType' => Entry::class,
                'criteria' => [
                    'section' => 'articles',
                    'orderBy' => 'postDate desc',
                    'limit' => 5,
                    'search' => extractQueryString(),
                ],
                'transformer' => function (Entry $entry) {
                    return [
                        'title' => $entry->title,
                        'url' => $entry->url,
                    ];
                },
                'paginate' => false,
            ];
        }
    ]
];