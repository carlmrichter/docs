const doc = document.documentElement;
const progressBar = document.querySelector('.article__progress');
let scrollPos = window.scrollY;

function updateProgress(scrollPos) {
    const scrollPercentage = 100 / (doc.offsetHeight - doc.clientHeight) * window.scrollY;
    doc.style.setProperty('--scroll', scrollPercentage.toFixed(2) + '%');
    if (scrollPercentage > 20) {
        progressBar.classList.remove('article__progress--hidden');
    } else {
        progressBar.classList.add('article__progress--hidden');
    }
}

window.addEventListener('scroll', function () {
    scrollPos = window.scrollY;
    if (doc.offsetHeight > 3 * doc.clientHeight) {
        updateProgress(scrollPos);
    }
});

document.querySelector('.restart').addEventListener('click', function () {
    document.documentElement.scrollTo({ top: 0, behavior: "smooth"});
});