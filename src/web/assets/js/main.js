const CLASS_NAV_MENU_OPEN = 'nav--open';

const nav = document.querySelector('.nav');
const navToggle = document.querySelector('.nav__toggle');

let viewportWidth = window.innerWidth;

function updateMenu() {
    if (viewportWidth > 1400) {
        nav.classList.add(CLASS_NAV_MENU_OPEN);
    } else {
        nav.classList.remove(CLASS_NAV_MENU_OPEN);
    }
}

function toggleMenu() {
    nav.classList.toggle(CLASS_NAV_MENU_OPEN);
}

function appendViewportWidth() {
    document.documentElement.style.setProperty('--vw', document.body.offsetWidth + 'px');
}

function update() {
    updateMenu();
    appendViewportWidth();
}

navToggle.addEventListener('click', function () {
    toggleMenu();
});

window.addEventListener('resize', function () {
    viewportWidth = window.innerWidth;
    update();
});

