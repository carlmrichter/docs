class Search {

    constructor(apiUrl) {
        this.apiUrl = apiUrl;
    }

    set apiUrl(url) {
        this._apiUrl = url;
    }

    get apiUrl() {
        return this._apiUrl;
    }

    static buildQueryString(params) {
        let str = [];
        for (let key in params) {
            if (!params.hasOwnProperty(key)) continue;
            str.push(encodeURIComponent(key) + '=' + encodeURIComponent(params[key]));
        }
        return str.join('&');
    }

    search(queryParams, callback) {
        if (typeof callback !== 'function' || typeof queryParams !== 'object') {
            return false;
        }
        let postRequest = new XMLHttpRequest();
        postRequest.open('GET', this.apiUrl + '?' + Search.buildQueryString(queryParams), true);
        postRequest.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                callback(JSON.parse(postRequest.responseText));
            }
        };
        postRequest.send();
    }
}

const searchElement = document.querySelector('.search');
const searchField = document.getElementById('searchField');
const resultsElem = document.querySelector('.search__results');
const navElement = document.querySelector('.nav');

searchField.addEventListener('input', function () {
   search.search({'q': searchField.value }, function (results) {
       let html = '';
       if (typeof results === 'object') {
           results.data.forEach(function (elem) {
               html += '<li class="search__result"<a href="' + elem.url + '">' + elem.title + '</a></li>'
           })
       }
       resultsElem.innerHTML = html;
   })
});

searchField.addEventListener('focus', function () {
    searchElement.classList.add('search--focused');
    navElement.classList.add('nav--hidden');
});

searchField.addEventListener('blur',  function () {
    searchElement.classList.remove('search--focused');
    navElement.classList.remove('nav--hidden');
});

const search = new Search('/api/search.json');